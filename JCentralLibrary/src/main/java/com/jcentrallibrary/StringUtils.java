package com.jcentrallibrary;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("unused")
public class StringUtils {
    public static String getCommaSeparatedString(String[] stringArray) {
        if (stringArray.length > 0) {
            StringBuilder nameBuilder = new StringBuilder();

            for (String n : stringArray) {
                nameBuilder.append("'").append(n.replace("'", "\\'")).append("',");
                // can also do the following
                // nameBuilder.append("'").append(n.replace("'", "''")).append("',");
            }

            nameBuilder.deleteCharAt(nameBuilder.length() - 1);

            return nameBuilder.toString().replace("'", "");
        } else {
            return null;
        }
    }

    public static String prefixNull(String str) {
        if (null == str) return null;
        return "NULL," + str;
    }

    public static String getCaptalisedWord(String str) {
        if (str.length() > 2)
            return str.substring(0, 1).toUpperCase(Locale.getDefault()) + str.substring(1).toLowerCase(Locale.getDefault());
        else
            return str;
    }

    public static boolean isNotNullAndNotEmpty(String reference) {
        return reference != null && reference.length() > 0;
    }

    public static String getFormattedStringRemovingSpecialCharacters(String str) {
        /*
        space with -
         / with -
         -- with -
         ' with (blank)
         , with (blank)
         " with (blank)
         \ with (blank)
         Or any other symbol with special characters with -
         */
        String result = null;
        if (null != str) {
            // ' , " \
            result = str.replaceAll("[\\'\\,\"\\\\]", "");

            // -- / and space
            result = result.replaceAll("--", "-");

            result = result.replaceAll("/", "-");

            result = result.replaceAll("\\s", "-");

            result = result.replaceAll("[^\\p{Alpha}\\p{Digit}]+", "-");

        }
        return result;
    }

    public static String getPipelineSeparatedString(String... strings) {
        List<String> stringList = new ArrayList<>(Arrays.asList(strings));
        stringList.removeAll(Arrays.asList("", null));
        return TextUtils.join("  |  ", stringList);
    }

    public static String getSeasonText(String season) {
        if (TextUtils.isEmpty(season)) return "";
        try {
            return String.format(Locale.ENGLISH, "S%02d", Integer.parseInt(season));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return String.format(Locale.ENGLISH, "S%s", season);
        }
    }


    public static String getEpisodeText(String episode) {
        if (TextUtils.isEmpty(episode)) return "";
        try {
            return String.format(Locale.ENGLISH, "E%02d", Integer.parseInt(episode));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return String.format(Locale.ENGLISH, "E%s", episode);
        }
    }

    public static String getSeasonText(String season, String episode, Boolean isKids) {
        if (TextUtils.isEmpty(getSeasonText(season)) && TextUtils.isEmpty(episode))
            return "";
        //don't display season name if genre is KIDS
        return String.format(Locale.ENGLISH, "%s %s", isKids ? "" : getSeasonText(season), getEpisodeText(episode));
    }

}

